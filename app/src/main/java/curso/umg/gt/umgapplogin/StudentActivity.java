package curso.umg.gt.umgapplogin;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class StudentActivity extends AppCompatActivity {

    Spinner spP4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);

        //Spinner pregunta 4
        spP4 = (Spinner) findViewById(R.id.spPregunta4);
        ArrayAdapter<CharSequence> adapterP4 = ArrayAdapter.createFromResource(this, R.array.opcionesP4, android.R.layout.simple_spinner_item);
        spP4.setAdapter(adapterP4);
    }
}
